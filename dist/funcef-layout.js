(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name Validate
    * @version 1.0.0
    * @Componente para validação de formulários
    */
    angular.module('funcef-layout.controller', []);
    angular.module('funcef-layout.directive', []);

    angular
    .module('funcef-layout', [
      'funcef-layout.controller',
      'funcef-layout.directive',
      'ngCookies'
    ]);
})();;(function () {
    'use strict';

    angular.module('funcef-layout.controller').
        controller('LayoutController', LayoutController);

    LayoutController.$inject = ['$rootScope', '$cookies', '$scope'];

    /* @ngInject */
    function LayoutController($rootScope, $cookies, $scope) {

        var vm = this;
        init();
        //////////

        console.log($scope.icons);

        function init() {
            $rootScope.layoutOpen = $cookies.get('layoutOpen') == 'true';
        }
    }
})();;(function () {
    'use strict';

    angular
      .module('funcef-layout.directive')
      .directive('ngfLayout', ngfLayout);

    /* @ngInject */
    function ngfLayout() {
        return {
            restrict: 'EA',
            replace: true,
            transclude: true,
            templateUrl: 'views/layout.view.html',
            controller: 'LayoutController',
            controllerAs: 'vm',
            scope: {
                options: '=',
                icons: '='
            },
            link: function (scope, el, attrs, ctrl, transclude) {
                el.find('.content').append(transclude());
            }
        };
    }
})();;angular.module('funcef-layout').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/layout.view.html',
    "<aside id=\"aside\" ng-show=\"options.length\"> <nav class=\"navbar navbar-black sidebar\" ng-class=\"{ 'open': $root.layoutOpen }\" role=\"navigation\"> <div class=\"container-fluid\"> <div class=\"collapse navbar-collapse\" id=\"bs-sidebar-navbar-collapse-1\"> <ul class=\"nav navbar-nav\"> <li ng-repeat=\"item in options track by $index\" class=\"dropdown\"> <a ui-sref=\"{{item.Itens.length ? '' : item.Descricao}}\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"> <i class=\"hidden-xs showopacity {{icons[item.Id]}}\"></i> <span class=\"item-text\">{{item.Nome}}</span> <span ng-if=\"item.Itens.length\" class=\"caret\"></span> </a> <ul role=\"layout\" ng-if=\"item.Itens.length\"> <li ng-repeat=\"itemLayout in item.Itens track by $index\"> <a ui-sref=\"{{itemLayout.Descricao}}\"> {{itemMenu.Nome}} </a> </li> </ul> </li> </ul> </div> </div> </nav> </aside>"
  );

}]);

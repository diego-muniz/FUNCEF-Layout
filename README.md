# Componente - FUNCEF-Layout

## Conteúdos

1. [Descrição](#descrição)
2. [Instalação](#instalação)
3. [Script](#script)
4. [CSS](#css)
5. [Auxiliares](#auxiliares)
6. [Desinstalação](#desinstalação)

## Descrição

- Componente Layout do sistema.

 
## Instalação:

### Bower

- Necessário executar o bower install e passar o nome do componente.

```
bower install funcef-layout --save
```

## Script

```html
<script src="bower_components/funcef-layout/dist/funcef-layout.js"></script>
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## CSS  

```html
<link rel="stylesheet" href="bower_components/funcef-layout/dist/funcef-layout.css">
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## Auxiliares

### Padding 

| **TODOS**  | **TOPO**  | **DIREITA**  | **BAIXO**  | **ESQUERDA**  |
| :---: | :---: | :---: | :---: | :---: |
|p-0 |pt-0 |pr-0 |pb-0 |pl-0|
|p-3 |pt-3 |pr-3 |pb-3 |pl-3|
|p-5 |pt-5 |pr-5 |pb-5 |pl-5|
|p-10|pt-10|pr-10|pb-10|pl-10|
|p-15|pt-15|pr-15|pb-15|pl-15|
|p-20|pt-20|pr-20|pb-20|pl-20|
|p-25|pt-25|pr-25|pb-25|pl-25|
|p-30|pt-30|pr-30|pb-30|pl-30|
|p-35|pt-35|pr-35|pb-35|pl-35|
|p-40|pt-40|pr-40|pb-40|pl-40|
|p-45|pt-45|pr-45|pb-45|pl-45|
|p-50|pt-50|pr-50|pb-50|pl-50|

### Margin 

| **TODOS**  | **TOPO**  | **DIREITA**  | **BAIXO**  | **ESQUERDA**  |
| :---: | :---: | :---: | :---: | :---: |
|m-0 |mt-0 |mr-0 |mb-0 |ml-0|
|m-3 |mt-3 |mr-3 |mb-3 |ml-3|
|m-5 |mt-5 |mr-5 |mb-5 |ml-5|
|m-10|mt-10|mr-10|mb-10|ml-10|
|m-15|mt-15|mr-15|mb-15|ml-15|
|m-20|mt-20|mr-20|mb-20|ml-20|
|m-25|mt-25|mr-25|mb-25|ml-25|
|m-30|mt-30|mr-30|mb-30|ml-30|
|m-35|mt-35|mr-35|mb-35|ml-35|
|m-40|mt-40|mr-40|mb-40|ml-40|
|m-45|mt-45|mr-45|mb-45|ml-45|
|m-50|mt-50|mr-50|mb-50|ml-50|

### Display flex
 - A propriedade flex especifica o comprimento do item em relação ao resto dos itens flexíveis dentro do mesmo container.
```html
   class="d-flex"
```

### Position relative
- Relativo a si mesmo.
```html
   class="p-relative"
```


### Border top none
- Remover bordar do topo.
```html
    class="border-top-none"
```

### Border bottom none
- Remover bordar do bottom.
```html
    class="border-bottom-none"
```


### Cursor pointer
- ponteiro. 
```html
    class="pointer"
```

### Tabela

| **Tabela**  | **Tabela TD Ellipsis**  |
| :---  | :---   |
| table |table-td-ellipsis |



### Estilo de texto

| **Negrito**  | **Empty**  | **Normal**  | **Ellipsis**  | 
| :---  | :---   | :---    | :--- |
| text-bold |text-empty |text-normal | ellipsis |



### Cores de texto

|  **Cor**  | **Demo**   | **Classe**  |
| :---  | :---   | :---    |
|Branco | ![#EFEFEF](https://placehold.it/15/EFEFEF/000000?text=+) | text-white |
|Preto  | ![#666](https://placehold.it/15/666/000000?text=+)       | text-black |

	



### Cores  fundo

|  **Cor**  | **Demo**   | **Classe**  |
| :--- |:---: | :--- |
|Vermelho           | ![#d9534f](https://placehold.it/15/d9534f/000000?text=+) | bg-red         |	                                                            
|Verde	            | ![#5cb85c](https://placehold.it/15/5cb85c/000000?text=+) | bg-green       |   
|Azul               | ![5bc0de](https://placehold.it/15/5bc0de/000000?text=+)  | bg-blue        |	                                                                
|Amarelo            | ![#f0ad4e](https://placehold.it/15/f0ad4e/000000?text=+) | bg-yellow      |
|Cinza Escuro       | ![#222](https://placehold.it/15/222/000000?text=+)       | bg-gray-darker |
|Cinza Sombrio      | ![#333](https://placehold.it/15/333/000000?text=+) 	   | bg-gray-dark   |                                                              
|Cinza              | ![#555](https://placehold.it/15/555/000000?text=+) 	   | bg-gray        |
|Cinza Luz	        | ![#777](https://placehold.it/15/777/000000?text=+)       | bg-gray-light  |
|Cinza Claro        | ![#eee](https://placehold.it/15/eee/000000?text=+)       | bg-gray-lighter|


### Tamanho

| **Classe** | **Width** | **Max-width**  |
| :---  |:---: | :---:    |
|max-width-135|      | 135px |
|width-130    |130px |       |
|w-10-pc      |10%   |       |
|w-33-pc      |33%   |       |


### Checkbox Center
```html
    <div class="checkbox3 checkbox-center checkbox-primary" 
         <input id="checkBoxCenter" type="checkbox" checked="checked">
         <label for="checkBoxCenter"></label>
    </div>
```

### checkbox break line label
```html
 <div class="form-group col-sm-12 checkbox3 checkbox-center checkbox-break-line checkbox-primary checkbox-sm">
    <div>
        <input type="checkbox"
               id="checkbox-break-line">
        <label for="checkbox-break-line" class="control-label"></label>
    </div>
  </div>
```
### Grid View
```html
    <div class="row grid-view">
        <div class="col-sm-6">
            <dl>
                <dt>Título</dt>
                <dd>Título</dd>
            </dl>
        </div>
    </div>
```

### Modal
```html
<div class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Titulo</h4>
            </div>
            
            <!-- Tipos de modal-body -->
            <div class="modal-body">
            </div>
            <div class="modal-body-fixed">
            </div>
            <div class="modal-body-300">
            </div>
            <div class="modal-body-350">
            </div>
            
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
```
## Desinstalação

```
bower uninstall funcef-layout  --save
```

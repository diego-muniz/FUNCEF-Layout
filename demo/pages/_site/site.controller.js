(function () {
    'use strict';
    
    angular.module('funcef-demo.controller')
        .controller('SiteController', SiteController);

    SiteController.$inject = ['SISTEMA', 'VERSION'];
    
    function SiteController(SISTEMA, VERSION)
    {
        var site = this;

        site.sistema = SISTEMA;
        site.versao = VERSION;
        site.ano = new Date().getFullYear();


        site.menu =  [
            {
                Id:1,
                Nome:"Tabela",
                Descricao:"Tabela exemplo",
                Itens: []
            },
            {
                Id:2,
                Nome:"Menu 2",
                Descricao:"Menu 2",
                Itens: [
                    {
                        Id: 21,
                        Nome: "Menu 2.1",
                        Descricao: "Menu 2.1 Descricao"
                    },
                    {
                        Id: 22,
                        Nome: "Menu 2.2",
                        Descricao: "Menu 1.2 Descricao"
                    }
                ]
            },
            {
                Id:3,
                Nome:"Menu 3",
                Descricao:"Menu 3",
                Itens: [
                    {
                        Id: 11,
                        Nome: "Menu 3.1",
                        Descricao: "Menu 3.1 Descricao"
                    },
                    {
                        Id: 12,
                        Nome: "Menu 3.2",
                        Descricao: "Menu 3.2 Descricao"
                    }
                ]
            }];       


            site.icons = {
                1: "fa fa-server",
                2: "fa fa-cog",
                3: "fa fa-tachometer"
            };
    }
})();
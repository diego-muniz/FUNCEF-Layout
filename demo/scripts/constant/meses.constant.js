﻿(function () {
    'use strict';

    angular.module('funcef-demo.configuration')
        .constant('MesesConstant', [
            { ID: 1, Nome: 'Janeiro' },
            { ID: 2, Nome: 'Fevereiro' },
            { ID: 3, Nome: 'Março' },
            { ID: 4, Nome: 'Abril' },
            { ID: 5, Nome: 'Maio' },
            { ID: 6, Nome: 'Junho' },
            { ID: 7, Nome: 'Julho' },
            { ID: 8, Nome: 'Agosto' },
            { ID: 9, Nome: 'Setembro' },
            { ID: 10, Nome: 'Outubro' },
            { ID: 11, Nome: 'Novembro' },
            { ID: 12, Nome: 'Dezembro' }
        ]);
})();
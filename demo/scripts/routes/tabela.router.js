﻿(function () {
    'use strict';

    angular.module('funcef-demo')
        .config(function ($stateProvider, $urlRouterProvider) {

            $urlRouterProvider.when('', '/');
            $urlRouterProvider.otherwise('/404');

            $stateProvider
                .state('tabela', {
                    url: '/',
                    templateUrl: 'pages/tabela/tabela.view.html',
                    controller: 'TabelaController',
                    notAuthenticate: true,
                    controllerAs: 'vm',
                })

        });
})();
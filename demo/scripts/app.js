﻿(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name FuncefDemo
    * @version 1.0.0
    * @Componente para teste de componentes
    */
    angular.module('funcef-demo.controller', []);
    angular.module('funcef-demo.configuration', []);

    angular
        .module('funcef-demo', [
            'funcef-demo.controller',
            'funcef-demo.configuration',
            'ui.router',
            'funcef-header',
            'funcef-menu',
            'funcef-footer'
        ]);
})();
﻿(function () {
    'use strict';

    angular.module('funcef-layout.controller').
        controller('LayoutController', LayoutController);

    LayoutController.$inject = ['$rootScope', '$cookies', '$scope'];

    /* @ngInject */
    function LayoutController($rootScope, $cookies, $scope) {

        var vm = this;
        init();
        //////////

        console.log($scope.icons);

        function init() {
            $rootScope.layoutOpen = $cookies.get('layoutOpen') == 'true';
        }
    }
})();
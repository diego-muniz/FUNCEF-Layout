﻿(function () {
    'use strict';

    angular
      .module('funcef-layout.directive')
      .directive('ngfLayout', ngfLayout);

    /* @ngInject */
    function ngfLayout() {
        return {
            restrict: 'EA',
            replace: true,
            transclude: true,
            templateUrl: 'views/layout.view.html',
            controller: 'LayoutController',
            controllerAs: 'vm',
            scope: {
                options: '=',
                icons: '='
            },
            link: function (scope, el, attrs, ctrl, transclude) {
                el.find('.content').append(transclude());
            }
        };
    }
})();
﻿(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name Validate
    * @version 1.0.0
    * @Componente para validação de formulários
    */
    angular.module('funcef-layout.controller', []);
    angular.module('funcef-layout.directive', []);

    angular
    .module('funcef-layout', [
      'funcef-layout.controller',
      'funcef-layout.directive',
      'ngCookies'
    ]);
})();